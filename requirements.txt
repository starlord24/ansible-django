Django>=2.1.3,<2.2.0
psycopg2-binary
django-extensions==2.2.8
python-dotenv
gunicorn